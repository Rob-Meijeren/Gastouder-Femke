<?php 

namespace App\Http\Controllers;

use DB;
use Hash;
use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Passport\TokenRepository;
use League\OAuth2\Server\ResourceServer;
use League\OAuth2\Server\Exception\OAuthServerException;
use Symfony\Bridge\PsrHttpMessage\Factory\DiactorosFactory;

class UserController extends Controller {

    protected $server;

    public function __construct(ResourceServer $server) {
        $this->server = $server;
    }

    private function getUserFromDatabase(Request $request) {
        $psr = (new DiactorosFactory)->createRequest($request);

        try {
            $psr = $this->server->validateAuthenticatedRequest($psr);

            $user = User::find($psr->getAttribute('oauth_user_id'));

            return $user;

        } catch (OAuthServerException $e) {
            return Container::getInstance()->make(
                ExceptionHandler::class
            )->report($e);
        }
    }
   
    public function getUser(Request $request) {
        $user = $this->getUserFromDatabase($request);

        return json_encode(array('user' => $user));
    }

    public function updateAccount(Request $request) {
        $user = $this->getUserFromDatabase($request);

        $user->name = $request->naam;
        $user->email = $request->email;
        $user->save();

        return json_encode(array('result' => 'Uw account is geupdated'));
    }

    public function updatePassword(Request $request) {
        $user = $this->getUserFromDatabase($request);

        if(Hash::check($request->current, $user->password)) {
            $user->password =  Hash::make($request->new);
            $user->save();

            return json_encode(array('result' => 'Uw wachtwoord is geupdated'));
        }
        else {
            return json_encode(array('error' => 'Uw huidige wachtwoord klopt niet'));
        }
    }

    public function registreer(Request $request) {
        $rules = array(
            'naam'=>'required',
            'email'=>'required|email',
            'wachtwoord'=>'required|regex:/^[\pL\s\d\.]+$/u',
        );
        
        $this->validate($request, $rules);
        
        $user = new User;
        $user->name = $request->naam;
        $user->email = $request->email;
        $user->password = Hash::make($request->wachtwoord);
        $user->save();
        
        return json_encode(array('result' => 'Uw account is aangemaakt'));
    }

    public function subscribeMaillist(Request $request) {
        $email = $request->email;
        $naam = $request->naam;

        if(!empty($email) && !empty($naam)) {
            $existing = Maillist::where('email', '=', $email)->first();

            if(is_null($existing)) {
                $mail = new Maillist();
                $mail->naam = $naam;
                $mail->email = $email;
                $mail->aantal_gedownloade_routes = 1;
                $mail->save();

                return json_encode(array('success' => 'new'));
            }
            else {
                $existing->aantal_gedownloade_routes = ($existing->aantal_gedownloade_routes + 1);
                $existing->save();
                return json_encode(array('success' => 'existing'));
            }
        }
        else {
            return json_encode(array('failure' => 'empty fields'));
        }
    }
}