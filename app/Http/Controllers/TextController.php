<?php

namespace App\Http\Controllers;

use App\Teksten;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TextController extends Controller {

	public function getAllPages(Request $request) {
		$pages = Teksten::select('page')->get()->toArray();
		
		return json_encode(array('pages' => $pages));
	}
	
	public function getText(Request $request){
		$dbTekst = Teksten::where('page', '=', $request->page)->get()->first();

		if(!is_null($dbTekst)) {
			return json_encode(array('text' => $dbTekst->content));
		}
	}
	
	public function updateText(Request $request){
		$text = Teksten::where('page', '=', $request->page)->get()->first();
		$text->content = $request->text;
		$text->save();

		return json_encode(array('updated' => 'De tekst is geupdated'));
	}

}