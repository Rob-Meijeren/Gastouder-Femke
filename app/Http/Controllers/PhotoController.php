<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Image;
use App\Foto;

class PhotoController extends Controller {

	public function getRandomPhotos(Request $request) {
		$numberOfPhotos = $request->numberOfPhotos || 5;

		$fotos = Foto::orderByRaw("RAND()")->take($numberOfPhotos)->get();

		return json_encode(array('photos' => $fotos));
	}

	public function getPhotosOfAlbum(Request $request) {
		$photos = Foto::where('album_id', '=', $request->albumId)->get();
		
		return json_encode(array('fotos' => $photos->toArray()));
	}
	
	public function uploadPhoto(Request $request){
		$saved = $this->savePhoto($request->albumId, $request->photo);

		if($saved) {
			return json_encode(array('photo' => $saved));
		}
		else {
			return json_encode(array('error' => 'photo not saved'));
		}
	}
	
	public function removePhoto(Request $request){
		$photoID = $request->photoId;
		$photo = Foto::find($photoID);
		$photo->delete();
	}

	private function savePhoto($albumID, $photo) {
		$directory = public_path() . '/img/'. $albumID;

		if (!file_exists($directory)) {
		    mkdir($directory);
		}

		$img = Image::make($photo['base64']);
		$img->filename = $photo['filename'];

		$photo = new Foto();
		$photo->album_id = $albumID;
		$photo->path = '/img/'. $albumID . '/' . $img->filename;
		$photo->save();
		$img->save($directory . '/' . $img->filename);
		return $photo;
	}

}