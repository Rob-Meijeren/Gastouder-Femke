<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;
use App\Foto;
use App\Teksten;
use App\Availability;


class HomeController extends Controller {

	public function getAvailability(Request $request) {
		$days = Availability::all();

		return json_encode(array('days' => $days->toArray()));
	}

	public function updateAvailability(Request $request) {
		$day = Availability::where('day', '=', $request->day)->first();

		$day->morning = $request->morning;
		$day->afternoon = $request->afternoon;
		$day->evening = $request->evening;
		$day->save();

		return json_encode(array('result' => 'De beschikbaarheid is geupdated'));
	}
	
	public function sendMessage(Request $request){
		$rules = array(
					'naam' => 'required|regex:/^[\pL\s\d\.]+$/u',
				    'email' => 'required|email',
				    'onderwerp' => 'required|regex:/^[\pL\s\d\.]+$/u',
				    'bericht' => 'required|regex:/^[\pL\s\d\.]+$/u'
				    );
		
		$this->validate($request, $rules);

	    Mail::send('emails.contactForm', array('name' => $request->naam, 'phone' => $request->telefoon, 'email' => $request->email, 'bericht' => $request->bericht), function($message) use ($request) {
	    	$message->from($request->email, $request->naam);
	    	$message->to('info@gastouderfemketilburg.nl')->subject($request->onderwerp);
	    });
	}
}