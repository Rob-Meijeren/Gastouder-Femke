<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\PhotoController;
use Illuminate\Http\Request;
use DB;
use App\Album;
use App\Foto;

class AlbumController extends Controller {
	
	public function addAlbum(Request $request, PhotoController $photoController){
		$album = new Album();
		$album->Naam = $request->naam;
		$album->Omschrijving = $request->omschrijving;
		$album->save();

		return json_encode(array('result' => 'Het album is aangemaakt', 'albumID' => $album->id));
	}

	public function getAllAlbums(){
		$albums = Album::all();
		
		return json_encode(array('albums' => $albums->toArray()));
	}
	
	public function getAlbum(Request $request){
		$album = Album::find($request->albumID);
		
		if(is_a($album, 'Album')){
			$photos = $album->fotos()->get();
			return json_encode(array('album' => $album->toArray(), 'fotos' => $photos->toArray()));
		}else{
			return json_encode(array('album' => 'geen geldig album'));
		}
	}
	
	public function updateGeneralAlbum(Request $request){
		$albumID = $request->albumID;
		$albumNaam = $request->albumNaam;
		$albumOmschrijving = $request->albumOmschrijving;
		
		$album = Album::find($albumID);
		$album->Naam = $albumNaam;
		$album->Omschrijving = $albumOmschrijving;
		$album->save();

		return json_encode(array('updated' => 'Het album is geupdated'));
	}

}