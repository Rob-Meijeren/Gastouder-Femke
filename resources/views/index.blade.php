<!DOCTYPE html>
<html>
	<head>
		<base href="/">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name='description' itemprop='description' content='Gastouder Femke, Ik (Femke Meijeren) ben een Gastouder in Tilburg die samenwerkt met Gastouderbureau Viaviela. Ik bied flexibele opvang waardoor u bijna altijd bij mij terecht kan.' />
	    <meta name='keywords' content='Gastouder, Tilburg, Flexibel, Gastouderbureau Viaviela, ochtendopvang, middagopvang, opvang, naschoolse opvang, Femke, Meijeren, Kinderopvang, Gastouder Femke, huiselijk, huiselijke sfeer, sfeer' />
		<title>Gastouder Femke</title>
		<link href="/css/bootstrap.css" rel="stylesheet" type="text/css" />
		<link href="/css/all.css" rel="stylesheet" type="text/css" />
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-50864554-5"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-50864554-5');
		</script>
	</head>
	<body ng-app="Femke">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class='navbar navbar-static-top' ng-controller='NavbarController as vm'>
						<div class='container'>
							<div class="nav-header">
								<a href='/' class='navbar-brand'><img src="/img/Logo.png" alt="Logo" class="img-responsive" /></a>
							</div>
							<div class="nav-body">
								<button class='navbar-toggle' data-toggle='collapse' data-target='.navHeaderCollapse'>
									Menu
								</button>
								<div class='collapse navbar-collapse navHeaderCollapse'>
										<ul class='nav navbar-nav' ng-if='!loggedIn'>
											<li><a href="/">Welkom</a></li>
											<li><a href="/overfemke">Over Femke</a></li>
											<li><a href="/opvanglocatie">De opvanglocatie</a></li>
											<li><a href="/activiteiten">Activiteiten</a></li>
											<li><a href="/fotoboek">Fotoboek</a></li>
											<li><a href="/contact">Contact</a></li>
										</ul>
										<ul class='nav navbar-nav' ng-if="loggedIn">
											<li><a href='/'>Home</a></li>
											<li><a href='/account'>Account</a></li>
											<li><a href='/teksten'>Pas teksten aan</a></li>
											<li role="presentation" class="dropdown">
		                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">Albums <span class="caret"></span></a>
		                                        <ul class="dropdown-menu" role="menu">
		                                           	<li><a href='/voegAlbumToe'>Voeg een album toe</a></li>
							               			<li><a href='/bewerkAlbum'>Bewerk een album</a></li>
		                                        </ul>
		                                    </li>
							                
							                <li><a ng-click="vm.logout()">Log uit</a></li>
										</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 content">
					<div ng-controller="ErrorController">
	                	<div class='col-md-6 col-md-offset-3'>
	                        <p class='alert alert-info' ng-show='result'>{{ result }}</p>
	                    </div>
	                </div>
					<div ng-view></div>	
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 footer">
					<div class="pull-left">
						&copy; Child Care - Theme by DTBAKER
					</div>
					<div class="pull-right">
						<a href="contact">Contact</a>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="/js/vendor.js"></script>
		<script type="text/javascript" src="/js/all.js"></script>
	</body>
</html>