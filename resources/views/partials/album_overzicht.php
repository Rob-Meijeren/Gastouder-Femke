<div class="row">
	<div class="col-md-12">
	<h3>Fotoboek</h3><br />
	<p>Om een idee van de opvang te krijgen nodig ik u uit om het fotoboek te bekijken.</p>
	<div ng-if="vm.albums.length === 0">
		<p>Er zijn nog geen albums toegevoegd.</p>
	<div>
	<div ng-repeat="album in vm.albums track by $index">
		<div class='col-md-4' ng-if="vm.album.fotos.length > 0">
			<h4><a href='/album/{{ vm.album.id }}'>{{ vm.album.Naam }}</a></h4><br>
				<div id='images{{ $index }}' class='carousel slide'>
					<div class='carousel-inner'>
						<div class='item active' ng-repeat="photo in vm.album.fotos" ng-class="'active': photo.active">
							<a href='/album/{{ vm.album.id }}'><img src='{{ photo.path }}' alt='Foto' class='img-responsive' /></a>
						</div>
					</div>
				</div>
				<div ng-if="vm.album.description.length > 0">
					{{ vm.album.description }}
				</div>
		</div>
	</div>
</div>
<script type="text/javascript">$(document).ready(function(){$("div[id^='images']").carousel({interval:3000,cycle:true});});</script>