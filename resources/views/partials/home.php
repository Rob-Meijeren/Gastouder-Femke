<div class="row">
	<div class="col-md-12">
		<div id='myCarousel1' class='carousel slide' ng-if="vm.photos.length > 0">
			<ol class='carousel-indicators'  ng-repeat="photo in vm.photos track by $index">
				<li data-target='#myCarousel1' data-slide-to='{{ $index }}' ng-class="'active': $index === 0;"></li>
			</ol>
			<div class='carousel-inner' ng-repeat="photo in vm.photos track by $index">
				<div class='item' ng-class="'active': $index === 0;">
					<a href='/gallerij'><img ng-src='{{ photo.path }}' alt='Foto' class='img-responsive' /></a>
				</div>
			</div>
			<a class='carousel-control carousel-control-home left' href='#myCarousel1' data-slide='prev' ><span class='glyphicon glyphicon-chevron-left'></span></a>
			<a class='carousel-control carousel-control-home right' href='#myCarousel1' data-slide='next' ><span class='glyphicon glyphicon-chevron-right'></span></a>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-4">
		<h3>Beschikbaarheid</h3><br>
		<table class="table" ng-init="vm.getAvailability()">
			<thead>
				<tr>
					<th></th>
					<th class="text-center">Ochtend</th>
					<th class="text-center">Middag</th>
					<th class="text-center">Avond</th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="day in vm.days">
					<td>{{ day.day }}</td>
					<td><img ng-src="{{ vm.getPictureSrc(day.morning) }}" class="center-block"></td>
					<td><img ng-src="{{ vm.getPictureSrc(day.afternoon) }}" class="center-block"></td>
					<td><img ng-src="{{ vm.getPictureSrc(day.evening) }}" class="center-block"></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-8">
		<div class="right_column" ng-init="vm.getText('home')">
			<h3>Welkom</h3><br>
			<p><img src="img/teddy.png" alt="Teddy" align="right" class="img-responsive" /></p>
			<div ng-bind-html="vm.text"></div>
		</div>
	</div>
</div>
<script type="text/javascript">$(document).ready(function(){$('#myCarousel1').carousel({interval:3000,cycle:true});});</script>