@extends('layout.index')

@section('content')
	<h1>{{ $album->Naam }}</h1>
	<p>{{ $album->Omschrijving }}</p>
	@for($i = 0; $i < count($fotos); $i++)
		@if(($i % 3) == 0)
			<div class='row'>
				<div class='col-md-4'>
					<a href='#' data-toggle="modal" data-target="#image-galery"><img src='{{ $fotos[$i]->Pad }}' width='350px' height='350px' alt='Foto' class='img-responsive' /></a>
					<br><h4>{{ $fotos[$i]->Titel }}</h4>
					<br><p>{{  $fotos[$i]->Omschrijving }}</p>
				</div>
		@elseif($i % 3 == 2)
				<div class='col-md-4'>
					<a href='#' data-toggle="modal" data-target="#image-galery"><img src='{{ $fotos[$i]->Pad }}' width='350px' height='350px' alt='Foto' class='img-responsive' /></a>
					<br><h4>{{ $fotos[$i]->Titel }}</h4>
					<br><p>{{  $fotos[$i]->Omschrijving }}</p>
				</div>
			</div>
		@else
			<div class='col-md-4'>
				<a href='#' data-toggle="modal" data-target="#image-galery"><img src='{{ $fotos[$i]->Pad }}' width='350px' height='350px' alt='Foto' class='img-responsive' /></a>
				<br><h4>{{ $fotos[$i]->Titel }}</h4>
				<br><p>{{  $fotos[$i]->Omschrijving }}</p>
			</div>
		@endif
	@endfor
@stop

@section('modals')
	<div class='modal slide' id='image-galery' role='dialog'>
		<div class='modal-dialog modal-lg'>
			<ol class='carousel-indicators' >
				@foreach($fotos as $index => $foto)
					@if($index == 0)
						<li data-target='#image-galery' data-slide-to='{{ $index }}' class='active' ></li>
					@else
						<li data-target='#image-galery' data-slide-to='{{ $index }}' ></li>
					@endif
				@endforeach
			</ol>
			<button class="close" data-dismiss="modal">&times;</button>
	        <div class="carousel-inner">
	        	@foreach($fotos as $index => $foto)
	        		@if($index == 0)
	        			<div class="item active">
			                <img src='{{ $foto->Pad }}' alt='Foto' class='img-responsive' />
			                <div class='carousel-caption'>
			                	<h3>{{ $foto->Titel }}</h3>
			                	<p>{{ $foto->Omschrijving }}</p>
			                </div>
			            </div>
	        		@else
	        			<div class="item">
			                <img src='{{ $foto->Pad }}' alt='Foto' class='img-responsive' />
			                <div class='carousel-caption'>
			                	<h3>{{ $foto->Titel }}</h3>
			                	<p>{{ $foto->Omschrijving }}</p>
			                </div>
			            </div>
	        		@endif
	        	@endforeach
	        </div>
	        
	        <!-- Carousel nav -->
	        <a class="carousel-control carousel-control-detail left" href='#image-galery' data-slide="prev"><span class='glyphicon glyphicon-chevron-left'></span></a>
	        <a class="carousel-control carousel-control-detail right" href='#image-galery' data-slide="next"><span class='glyphicon glyphicon-chevron-right'></span></a>
		</div>
    </div>
@stop

@section('scripts')
	<script type="text/javascript">$(document).ready(function(){$('#image-galery').carousel({interval:3000,cycle:true});});</script>
@stop