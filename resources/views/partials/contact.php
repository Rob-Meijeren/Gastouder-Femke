<div class="row">
	<div class="col-md-4 text-left">
		<h5>Gastouder Femke</h5>
		<p>
			<span class="contact_title">Tel:</span> <span class="contact_detail">013-4553599</span><br>
			<span class="contact_title">Mob:</span><span class="contact_detail">06-29435984</span><br>
		</p>
		<br>
        <p>
        	<h5>Adres:</h5>
			<p>
				Schiphollaan 66<br />
				5042 TR Tilburg
			</p> 
		</p>
	</div>
	<div class="col-md-4 col-md-offset-1">
		<h3>Contact</h3>
		<p><b>Om mij te bereiken kunt u bellen met het telefoonnummer hiernaast of het formulier hieronder invullen, ik neem dan zo snel mogelijk contact met u op.</b></p>
		<form class='form-horizontal' ng-submit="vm.sendContactForm()">
			<fieldset>
	        	<div class='form-group'>
					<label>Naam</label> *
					<input type='text' ng-model='vm.Naam' placeholder='Uw Naam' class='form-control' required />
				</div>
				<div class='form-group'>
					<label>Email</label> *
					<input type='email' ng-model='vm.Email' placeholder='Uw emailadres' class='form-control' required />
				</div>
				<div class='form-group'>
					<label>Telefoonnr</label>
					<input type='text' ng-model='vm.Telefoon' placeholder='0612345678' class='form-control' />
				</div>
				<div class='form-group'>
					<label>Onderwerp</label> *
					<input type='text' ng-model='vm.Onderwerp' placeholder='Het onderwerp' class='form-control' required />
				</div>
				<div class='form-group'>
					<label>Uw Bericht</label> *
					<textarea class='form-control' ng-model='vm.Bericht' cols='30' rows='4' required></textarea>
				</div>
				<div class='form-group'>
					* = verplicht
				</div>
	        </fieldset>
		    <button type="submit" class="btn btn-primary">Verstuur</button>
		</form>
	</div>
</div>