<div class="row" ng-init='vm.getAuthenticatedUser()'>
	<div class="col-md-6">
		<h3>Account</h3>
		<form name="accountForm" ng-submit="vm.updateAccount()">
	        <fieldset>
	        	<div class='col-md-12'>
		        	<div class='form-group'>
						<label>Uw naam</label>
						<input type='text' ng-model='vm.user.name' class='form-control black' />
					</div>
					<div class='form-group'>
						<label>Email</label>
						<input type='text' ng-model='vm.user.email' class='form-control black' />
					</div>
				</div>
			</fieldset>
	        <button type="submit" class="btn btn-primary">Update account</button>
		</form>
	</div>
	<div class="col-md-6">
		<h3>Beschikbaarheid</h3>
		<table class="table" ng-init="vm.getAvailability()">
			<thead>
				<tr>
					<th class="text-center">Dag</th>
					<th class="text-center">Ochtend</th>
					<th class="text-center">Middag</th>
					<th class="text-center">Avond</th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="day in vm.days">
					<td class="text-center">{{ day.day }}</td>
					<td class="text-center"><input type="checkbox" ng-checked="day.morning" ng-model="day.morning" ng-change="vm.updateDay(day)" class="checkbox-center"></td>
					<td class="text-center"><input type="checkbox" ng-checked="day.afternoon" ng-model="day.afternoon" ng-change="vm.updateDay(day)" class="checkbox-center"></td>
					<td class="text-center"><input type="checkbox" ng-checked="day.evening" ng-model="day.evening" ng-change="vm.updateDay(day)" class="checkbox-center"></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<h3>Wachtwoord veranderen</h3>
		<form name="wwForm" novalidate ng-submit="wwForm.$valid && vm.updatePassword()">
			<fieldset>
	        	<div class='form-group'>
					<label>Huidig wachtwoord</label> *
					<input type='password' ng-model='vm.currentPassword' class='form-control black' required />
				</div>
				<div class='form-group'>
					<label>Nieuw wachtwoord</label> *
					<input type='password' ng-model='vm.newPassword' id='password' class='form-control black' required />
				</div>
				<div class='form-group'>
					<label>Nieuw wachtwoord bevestigen</label> *
					<input type='password' ng-model='vm.passwordConfirmation' name='confirmPassword' password-verify='vm.newPassword' class='form-control black' required />
					<span ng-show="wwForm.confirmPassword.$error.passwordVerify">De wachtwoorden komen niet overeen</span>
				</div>
			</fieldset>
	        <button type="submit" class="btn btn-primary">Verander wachtwoord</button>
	    </form>
	</div>
	<div class="col-md-6">
		<h3>Registreer een nieuwe gebruiker</h3>
		<form name="createUserForm" novalidate ng-submit="createUserForm.$valid && vm.registreer()">
			<fieldset>
	        	<div class='form-group'>
					<label>Naam</label>
					<input type='text' ng-model='vm.naam' class='form-control black' required />
				</div>
				<div class='form-group'>
					<label>Email</label>
					<input type='text' ng-model='vm.email' id='email' class='form-control black' required />
				</div>
				<div class='form-group'>
					<label>Wachtwoord</label>
					<input type='password' ng-model='vm.newPassword' class='form-control black' required />
				</div>
			</fieldset>
	        <button type="submit" class="btn btn-primary">Registreer gebruiker</button>
	    </form>
	</div>
</div>