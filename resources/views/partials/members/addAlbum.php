<div class="row">
	<div class="col-md-12">
		<form name="albumForm" ng-submit="vm.createAlbum()">
	        <fieldset>
	        	<div class='col-md-5'>
		        	<h3>Album Details</h3>
		        	<div class='form-group'>
						<label>Album Naam</label> *
						<input type='text' ng-model='vm.albumNaam' placeholder='Album naam' class='form-control' required />
					</div>
					<div class='form-group'>
						<label>Album Omschrijving</label>
						<textarea class='form-control' ng-model='vm.albumOmschrijving' cols='30' rows='4'></textarea>
					</div>
					<div class='form-group'>
						* = verplicht
					</div>
				</div>
				<div class='col-md-9'> 
					<h3>Foto's voor het album</h3>
					<div class='files'>
						<div class='photo'>
							<p><b>Let op:</b> je kan meerdere bestanden tegelijk selecteren. Houd Ctrl ingedrukt tijdens het selecteren om meerdere te selecteren.</p>
							<input type="file" ng-model='vm.albumPhotos' accept='image/*' multiple base-sixty-four-input/><br />
						</div>
					</div>
				</div>
	        </fieldset>
	        <button type="submit" class="btn btn-primary">Maak een nieuw album</button>
	    </form>
	</div>
</div>