<div class="row">
	<div class="col-md-12" ng-init="vm.getSections()">
		Selecteer een pagina om de tekst ervan aan te passen:
		<select name="textSelect" class='black' ng-model='vm.selectedPage'>
			 <option ng-repeat="item in vm.pages" value="{{item.page}}">{{item.page}}</option>
		</select><br />

		<div ng-show='vm.selectedPage'>
			<h3>Huidige Tekst</h3>
			
			<textarea cols="100" rows="20" ng-model="vm.text"></textarea><br />
			
			<input type='submit' value="Update" class='btn btn-primary' ng-click='vm.updateText()' />
		</div>
	</div>
</div>