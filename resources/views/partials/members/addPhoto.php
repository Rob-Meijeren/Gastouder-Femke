<div ng-show='loggedIn === null' class="col-md-10">
	U moet ingelogd zijn om deze pagina te kunnen zien.
</div>
<div ng-show='loggedIn !== null' class='col-md-10' ng-init='vm.getAllAlbums();'>
	<div class="row">
		<div class="col-md-12">
			<img class="loading" src="/img/preloader.gif" ng-show="vm.loading" />
		</div>
		<div class="col-md-12" ng-show="!vm.loading">
			Selecteer een album om fotos aan toe te voegen:
			<select name="albumSelect" class='black' ng-model='vm.selectedAlbum' ng-options="album.Naam for album in vm.albums track by album.id"></select>
		</div>
	</div>

	<div class="col-md-12" ng-show='vm.selectedAlbum'>
		<h3>Huidige Foto's</h3>
		
		<div ng-if='photos.length === 0'>Er zijn nog geen fotos in dit album</div>
		<div class='recentPhotos' ng-repeat='photo in vm.selectedAlbum.fotos track by photo.id'>
			<div class='photo'>
				<img ng-src="{{ photo.path }}" class='large-thumb' /><br />
				<button class="btn btn-danger" ng-click='vm.removePhotoFromAlbum(photo)'>Verwijder Foto</button>
			</div>
		</div>
		<br />
		<h3>Voeg nieuwe foto's toe</h3>
		<div class="photo">	
			<img id="mySpinner" src="/img/preloader.gif" ng-show="vm.uploading" />
			<div ng-show='!vm.uploading'>
				<p><b>Let op:</b> je kan meerdere bestanden tegelijk selecteren. Houd Ctrl ingedrukt tijdens het selecteren om meerdere te selecteren.</p>
				<input type="file" ng-model='vm.newPhotos' accept='image/*' multiple base-sixty-four-input/><br />
			</div>
		</div>
		<input type='submit' value="Voeg de foto's aan het project toe" class='btn btn-success' ng-if='vm.newPhotos.length > 0' ng-click='vm.addPhotoToAlbum(vm.selectedAlbum.id, vm.newPhotos)' />
	</div>
</div>