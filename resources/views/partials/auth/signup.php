<div class='col-md-10 col-md-offset-2'>
    <form name="loginForm" ng-submit="vm.registreer()">
        <div class="form-group">
            <div class="col-md-3">
                <input type="text" ng-model="vm.naam" class="form-control" placeholder="Username" required>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-3">
                <input type="email" ng-model="vm.email" class="form-control" placeholder="Email" required>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-3">
                <input type="text" ng-model="vm.newPassword" class="form-control" placeholder="Password" required>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-4">
                <button type="submit" class="btn btn-primary">Sign Up</button>
            </div>
        </div>
    </form>
</div>
