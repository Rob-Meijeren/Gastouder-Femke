<div class='col-md-5'>
    <form class='form-horizontal' ng-submit="vm.login()">
        <fieldset>
            <div class='form-group'>
                <label>Email</label>
                <input type='email' ng-model='vm.email' placeholder='Uw emailadres' class='form-control' required />
            </div>
            <div class='form-group'>
                <label>Wachtwoord</label>
                <input type="password" id="password" ng-model="vm.password" class="form-control" placeholder="Wachtwoord" />
            </div>
        </fieldset>
        <button type="submit" class="btn btn-primary">Login</button>
    </form>
</div>
