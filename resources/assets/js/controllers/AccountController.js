angular.module('AccountController', ['UserService', 'AvailabilityService'])
.controller('AccountController', ['$rootScope', '$scope', 'User', 'Availability', AccountController]);

function AccountController ($rootScope, $scope, UserService, AvailabilityService) {
	var vm = this;

	vm.user;
	vm.currentPassword;
	vm.newPassword;
	vm.passwordConfirmation;
	vm.email;
	vm.naam;
	vm.days;

	vm.getAvailability = function () {
        AvailabilityService.getAvailability().then(function (response) {
            vm.days = response.days;
        });
    };

    vm.updateDay = function (day) {
    	AvailabilityService.updateDay(day).then(function (response) {
    		$rootScope.result = response.result;
    	});
    }

	vm.getAuthenticatedUser = function () {
		UserService.getUser().then(function (response) {
			vm.user = response.user;
		}, function (error) {
			$rootScope.result = error;
		})
	}

	vm.updateAccount = function () {
		UserService.update(vm.user.name, vm.user.email).then(function (response) {
			$rootScope.result = response.result;
		}, function (error) {
			$rootScope.result = error;
		});
	};

	vm.updatePassword = function () {
		UserService.updatePassword(vm.user.id, vm.currentPassword, vm.newPassword).then(function (response) {
			if(response.error) {
				$rootScope.result = response.error;
			}
			else if(response.result) {
				$rootScope.result = response.result;
				vm.currentPassword = '';
				vm.newPassword = '';
				vm.passwordConfirmation = '';
			}
		}, function (error) {
			$rootScope.result = error;
		});
	};

	vm.registreer = function () {
		debugger;
		UserService.create(vm.naam, vm.email, vm.newPassword).then(function (response) {
			$rootScope.result = 'Gebruiker aangemaakt';
		}, function (error) {
			$rootScope.result = error;
		});
	}
}