angular.module('AlbumController', ['AlbumService', 'PhotoService', 'naif.base64', 'ui.bootstrap', 'ngAnimate', 'ngTouch'])
    .controller('AlbumController', ['$rootScope', '$scope', '$location', '$routeParams', '$q', '$window', 'Album', 'Photo', AlbumController]);

function AlbumController($rootScope, $scope, $location, $routeParams, $q, $window, AlbumService, PhotoService) {
    var vm = this;

    vm.albums = [];
    vm.selectedAlbum;
    vm.uploading = false;
    vm.loading = false;
    vm.albumNr = $routeParams.albumId;
    vm.albumPhotos = [];
    vm.albumNaam;
    vm.albumOmschrijving;
    vm.newPhotos;
    vm.activeSlide = 0;
    vm.thumbnailSize = 5;
 	vm.thumbnailPage = 1;
 	vm.thumbnails;

    vm.getAllAlbums = function() {
        vm.loading = true;
        AlbumService.getAllAlbums().then(function(response) {
            vm.albums = response.albums;
            var photoCalls = [];

            //get photos for each album concurrently
            angular.forEach(vm.albums, function(album, index) {
                var photoCall = getPhotosofAlbum(album).then(function(photos) {
                    album.fotos = photos;
                    if(album.fotos.length > 0){
                        album.fotos[0].active = true;
                    }
                }, function(error) {
                    album.fotos = [error];
                });
                photoCalls.push(photoCall);
            });

            $q.all(photoCalls).then(function() {
                vm.loading = false;
            }, function() {
                vm.loading = false;
                $rootScope.result = 'Er is iets fout gegaan met het ophalen van de foto(s). Probeer het nogmaals of contacteer de webmaster';
            });
        }, function(response) {
            $rootScope.result = 'Er is iets fout gegaan met het ophalen van de albums. Laad de pagina nogmaals of contacteer de webmaster.';
        });
    };

    vm.getSpecificAlbum = function(albumId) {
        var albumID = albumId || vm.albumNr;
        AlbumService.getSpecificAlbum(albumID).then(function(response) {
            vm.selectedAlbum = response.album;

            getPhotosofAlbum(vm.selectedAlbum).then(function(photos) {
                vm.selectedAlbum.fotos = photos;
                vm.setThumbNails();
            });
        });
    };

    vm.addPhotoToAlbum = function(albumID, photos) {
        var promises = [];
        // upload a photo to the backend with the albumid when multiple pictures make multiple calls
        vm.uploading = true;
        angular.forEach(photos, function(photo) {
            var promise = PhotoService.uploadPhoto(albumID, photo).then(function(response) {
                if (typeof vm.selectedAlbum !== 'undefined') {
                    vm.selectedAlbum.fotos.push(response.photo);
                }
            });
            promises.push(promise);
        });

        $q.all(promises).then(function() {
            vm.uploading = false;
        }, function() {
            vm.uploading = false;
            $rootScope.result = 'Er is iets fout gegaan met het toevoegen van de foto(s). Probeer het nogmaals of contacteer de webmaster';
        })
    };

    vm.removePhotoFromAlbum = function(photo) {
        PhotoService.removePhoto(photo.id).then(function(response) {
            vm.selectedAlbum.fotos.splice(vm.selectedAlbum.fotos.indexOf(photo), 1);
        }, function(error) {
            $rootScope.result = error;
        });
    };

    vm.createAlbum = function() {
        // send the name and description of the newly to be created album to the backend
        AlbumService.createAlbum(vm.albumNaam, vm.albumOmschrijving).then(function(response) {
            vm.addPhotoToAlbum(response.albumID, vm.albumPhotos);
            $location.path('/bewerkAlbum/' + response.albumID);
        }, function(error) {
            console.log('Error');
        });
    };

    vm.prevPage = function(){
		if (vm.thumbnailPage > 1){
		  vm.thumbnailPage--;
		}
		vm.setThumbNails();
	}

	vm.nextPage = function(){
		if (vm.thumbnailPage <= Math.floor(vm.selectedAlbum.fotos.length/vm.thumbnailSize)){
		  vm.thumbnailPage++;
		}
		vm.setThumbNails();
	}

	vm.setThumbNails = function () {
  		vm.thumbnails = vm.selectedAlbum.fotos.slice((vm.thumbnailPage - 1) * vm.thumbnailSize, vm.thumbnailPage * vm.thumbnailSize);
  	}

	vm.setActive = function(idx) {
		vm.activeSlide = idx + ((vm.thumbnailPage - 1) * vm.thumbnailSize);
	}

    function getPhotosofAlbum(album) {
        var deferred = $q.defer();

        // get the photos of the album and add them to the album.
        PhotoService.getPhotosOfAlbum(album.id).then(function(response) {
            deferred.resolve(response.fotos);
        }, function(error) {
            deferred.reject('Niet alle fotos konden worden opgehaald.');
        });

        return deferred.promise;
    };
};
