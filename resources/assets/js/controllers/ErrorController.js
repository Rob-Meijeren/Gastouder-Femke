angular.module('ErrorController', []).controller('ErrorController', ['$rootScope', '$timeout', ErrorController]);

function ErrorController ($rootScope, $timeout) {
	$rootScope.result;

	$rootScope.$watch('result', function() {
        $timeout(function () {
        	if($rootScope.result !== '') {
        		$rootScope.result = '';
        	}
        }, 10000);
    });
}