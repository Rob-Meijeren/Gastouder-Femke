angular.module('ContactController', ['ContactService']).controller('ContactController', ['$rootScope', 'Contact', ContactController]);

function ContactController ($rootScope, ContactService) {
	var vm = this;

    vm.Naam;
	vm.Email;
	vm.Telefoon;
	vm.Onderwerp;
	vm.Bericht;

	vm.sendContactForm = function () {
		ContactService.contact(vm.Naam, vm.Email, vm.Telefoon, vm.Onderwerp, vm.Bericht).then(function () {
			$rootScope.result = 'Uw Email is verzonden. Er word zo snel mogelijk contact met u opgenomen.';
		}, function () {
			$rootScope.result = 'Er is een fout opgetreden bij het verzenden van uw email. Weet u zeker dat u de verplichte velden hebt ingevuld? Probeer het later nog eens. Blijft het probleem bestaan kunt u de webmaster bereiken op webmaster@vmtr.nl';
		});
	};
};