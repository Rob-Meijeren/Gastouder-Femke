angular.module('UserController', ['UserService', 'ngStorage'])
    .controller('UserController', ['$rootScope', '$window', '$scope', '$localStorage', '$location', 'User', UserController]);

function UserController ($rootScope, $window, $scope, $localStorage, $location, userService) {
    var vm = this;
    vm.email;
    vm.password;
    vm.result;

    vm.login = function () {
        userService.login(vm.email, vm.password).then(function (response) {
            if(response.login_error){
                $rootScope.result = response.login_error;
            }
            else {
                $localStorage.access_token = response.access_token;
                $window.sessionStorage.loggedIn = true;
                $location.path('/');
            }
        }, function (error) {
            $rootScope.result = 'Deze combinatie van email en wachtwoord bestaat niet in de database. Probeer het nogmaals met de juiste inloggegevens';
        })
    };
};
