angular.module('TekstenController', ['TextService'])
.controller('TekstenController', ['$rootScope', '$scope', 'Text', TekstenController]);

function TekstenController ($rootScope, $scope, textService) {
	var vm = this;

	vm.pages = [];
	vm.selectedPage;
	vm.text;

	vm.getSections = function () {
		textService.getPages().then(function (response) {
			vm.pages = response.pages;
		}, function (error) {
			$rootScope.result = error;
		});
	};

	vm.getTextOfSection = function () {
		textService.getText(vm.selectedPage).then(function (response) {
			vm.text = response.text;
		}, function (error) {
			$rootScope.result = error;
		});
	}

	vm.updateText = function () {
		textService.updateText(vm.selectedPage, vm.text).then(function (response) {
			$rootScope.result = response.updated;
		}, function (error) {
			$rootScope.result = error;
		});
	};

	$scope.$watch('vm.selectedPage', function () {
		vm.getTextOfSection();
	});
}