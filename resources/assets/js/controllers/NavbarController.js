angular.module('NavbarController', []).controller('NavbarController', ['$scope', '$window', '$localStorage', NavbarController]);

function NavbarController($scope, $window, $localStorage) {
	var vm = this; 

	$scope.$watch(function () {
        return $window.sessionStorage.loggedIn 
    }, function(n,o){
        vm.setLoggedIn();
    });

    vm.setLoggedIn = function () {
    	$scope.loggedIn = $window.sessionStorage.loggedIn;
    }

    vm.logout = function () {
    	delete $window.sessionStorage.loggedIn;
    	delete $localStorage.access_token;
    	delete $localStorage.refresh_token;
    }

    vm.setLoggedIn();
}