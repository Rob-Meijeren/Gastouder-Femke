var app = angular.module('MainController', ['ngSanitize', 'UserService', 'PhotoService', 'TextService', 'AvailabilityService'])

app.controller('MainController', ['$sce', '$localStorage', 'User', 'Photo', 'Text', 'Availability', MainController]);

function MainController ($sce, $localStorage, UserService, PhotoService, TextService, AvailabilityService) {
    var vm = this;

    vm.photos = [];

    vm.getRandomPictures = function () {
        PhotoService.getRandomPhotos(5).then(function (response) {
            vm.photos = response.photos;
        });
    };

    vm.getPictureSrc = function (value) {
        if(value === 0) {
            return '/img/kruisje.png';
        }
        else {
            return '/img/vinkje.png';
        }
    };

    vm.getText = function (page) {
        TextService.getText(page).then(function (response) {
            vm.text =  $sce.trustAsHtml(response.text);
        });
    };

    vm.getAvailability = function () {
        AvailabilityService.getAvailability().then(function (response) {
            vm.days = response.days;
        })
    };

/*    if($localStorage.access_token) {
        
    }*/
};