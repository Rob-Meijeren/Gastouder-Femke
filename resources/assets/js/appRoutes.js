angular.module('appRoutes', ['ngRoute', 'UserService']).config(['$routeProvider', '$locationProvider',
    function ($routeProvider, $locationProvider) {
        var templateHome = '/views';

        var authenticate = ['$window', '$localStorage', 'User', function ($window, $localStorage, userService) {
            return userService.validate().then(function (response) {
                if(!response.authenticated) {
                    delete $window.sessionStorage.loggedIn;
                    delete $localStorage.access_token;
                    return '/';
                }
                else {
                    return undefined;
                }
            }, function (error) {
                return '/';
            });
        }]

        $routeProvider
        .when('/', {
            templateUrl: templateHome + '/home',
            controller: 'MainController',
            controllerAs: 'vm'
        })
        .when('/overfemke', {
            templateUrl: templateHome + '/about',
            controller: 'MainController',
            controllerAs: 'vm'
        })
        .when('/opvanglocatie', {
            templateUrl: templateHome + '/daycare',
            controller: 'MainController',
            controllerAs: 'vm'
        })
        .when('/activiteiten', {
            templateUrl: templateHome + '/activities',
            controller: 'MainController',
            controllerAs: 'vm'
        })
        .when('/fotoboek', {
            templateUrl: templateHome + '/album_overzicht',
            controller: 'AlbumController',
            controllerAs: 'vm'
        })
        .when('/album/:albumId', {
            templateUrl: templateHome + '/album_detail',
            controller: 'AlbumController',
            controllerAs: 'vm'
        })
        .when('/contact', {
            templateUrl: templateHome + '/contact',
            controller: 'ContactController',
            controllerAs: 'vm'
        })
        .when('/voegAlbumToe', {
            templateUrl: templateHome + '/members/addAlbum',
            controller: 'AlbumController',
            controllerAs: 'vm',
            resolveRedirectTo: authenticate
        })
        .when('/bewerkAlbum/:albumId?', {
            templateUrl: templateHome + '/members/addPhoto',
            controller: 'AlbumController',
            controllerAs: 'vm',
            resolveRedirectTo: authenticate
        })
        .when('/account', {
            templateUrl: templateHome + '/members/account',
            controller: 'AccountController',
            controllerAs: 'vm',
            resolveRedirectTo: authenticate
        })
        .when('/teksten', {
            templateUrl: templateHome + '/members/teksten',
            controller: 'TekstenController',
            controllerAs: 'vm',
            resolveRedirectTo: authenticate
        })
        .when('/login', {
            templateUrl: templateHome + '/auth/login',
            controller: 'UserController',
            controllerAs: 'vm'
        })
        .otherwise({
            redirectTo: '/'
        });

        $locationProvider.html5Mode({
            enabled: true
        });
    }
]);
