angular.module('AlbumService', ['ngResource']).factory('AlbumResource', ['$resource',
    function($resource) {
        return $resource('/album', {}, {
            getAllAlbums: {
                method: 'GET',
                url: '/api/album/all'
            },
            getSpecificAlbum: {
                method: 'GET',
                url: '/api/album/:albumId'
            },
            removeAlbum: {
                method: 'POST',
                url: '/api/album/remove'
            },
            updateAlbum: {
                method: 'POST',
                url: '/api/album/update'
            },
            createAlbum: {
                method: 'POST',
                url: '/api/album/create'
            }
        });
    }
]).service('Album', ["$q", "AlbumResource", function ($q, AlbumResource) {

    function getAllAlbums() {
        var deferred = $q.defer();

        var request = new AlbumResource();

        request.$getAllAlbums(function (response) {
            deferred.resolve(response);
        }, function (response) {
            deferred.reject();
        });

        return deferred.promise;
    }

    function getSpecificAlbum(albumId) {
        var deferred = $q.defer();

        var request = new AlbumResource();

        request.$getSpecificAlbum({ albumId:albumId }, function (response) {
            deferred.resolve(response);
        }, function (response) {
            deferred.reject();
        });

        return deferred.promise;
    }

    function removeAlbum(albumId) {
        var deferred = $q.defer();

        var request = new AlbumResource();
        request.albumId = albumId;

        request.$removeAlbum(function (response) {
            deferred.resolve(response);
        }, function (response) {
            deferred.reject();
        });

        return deferred.promise;
    }

    function updateAlbum(albumId, Naam, Omschrijving) {
        var deferred = $q.defer();

        var request = new AlbumResource();
        request.albumID = albumId;
        request.naam = Naam;
        request.omschrijving = Omschrijving;

        request.$updateAlbum(function (response) {
            deferred.resolve(response);
        }, function (response) {
            deferred.reject();
        });

        return deferred.promise;
    }

    function createAlbum(Naam, Omschrijving) {
        var deferred = $q.defer();

        var request = new AlbumResource();
        request.naam = Naam;
        request.omschrijving = Omschrijving;

        request.$createAlbum(function (response) {
            deferred.resolve(response);
        }, function (response) {
            deferred.reject();
        });

        return deferred.promise;
    }

    return {
        "getAllAlbums": getAllAlbums,
        "getSpecificAlbum": getSpecificAlbum,
        "removeAlbum": removeAlbum,
        "updateAlbum": updateAlbum,
        "createAlbum": createAlbum
    };
}]);