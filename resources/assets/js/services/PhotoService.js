angular.module('PhotoService', ['ngResource']).factory('PhotoResource', ['$resource', '$localStorage',
    function($resource, $localStorage) {
        return $resource('/api/photo', {}, {
            getPhotosOfAlbum: {
                method: 'GET',
                url: '/api/photo/:albumId'
            },
            uploadPhoto: {
                method: 'POST',
                url: '/api/photo/add',
                headers: { 
                    'Accept': 'application/json',
                    'Authorization': function (config) { return 'Bearer ' + $localStorage.access_token }
                }
            },
            removePhoto: {
                method: 'POST',
                url: '/api/photo/remove',
                headers: { 
                    'Accept': 'application/json',
                    'Authorization': function (config) { return 'Bearer ' + $localStorage.access_token }
                }
            },
            getRandomPhotos: {
                method: 'GET',
                url: '/api/photo/random'
            }
        });
    }
]).service('Photo', ['$q', 'PhotoResource', function ($q, PhotoResource) {

        function getPhotosOfAlbum(albumId) {
            var deferred = $q.defer();

            var request = new PhotoResource();

            return request.$getPhotosOfAlbum({ albumId:albumId }, function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject();
            });

            return deferred.promise;
        }

        function uploadPhoto(albumId, photo) {
            var deferred = $q.defer();

            var request = new PhotoResource();
            request.albumId = albumId;
            request.photo = photo;

            return request.$uploadPhoto(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject();
            });

            return deferred.promise;
        }

        function removePhoto(photoId) {
            var deferred = $q.defer();

            var request = new PhotoResource();
            request.photoId = photoId;

            return request.$removePhoto(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject();
            });

            return deferred.promise;
        }

        function getRandomPhotos(amountOfPhotos) {
            var deferred = $q.defer();

            var request = new PhotoResource();
            request.numberOfPhotos = amountOfPhotos;

            return request.$getRandomPhotos(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject();
            });

            return deferred.promise;
        }

        return {
            "getPhotosOfAlbum": getPhotosOfAlbum,
            "uploadPhoto": uploadPhoto,
            "removePhoto": removePhoto,
            "getRandomPhotos": getRandomPhotos
        };
}]);