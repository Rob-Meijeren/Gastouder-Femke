angular.module('TextService', ['ngResource']).factory('TextResource', ['$resource', '$localStorage',
    function($resource, $localStorage) {
        return $resource('/text', {}, {
            getText: {
                method: 'GET',
                url: '/api/text/:page',
                headers: { 
                    'Accept': 'application/json',
                    'Authorization': function (config) { return 'Bearer ' + $localStorage.access_token }
                }
            },
            getPages: {
                method: 'GET',
                url: '/api/text/sections',
                headers: { 
                    'Accept': 'application/json',
                    'Authorization': function (config) { return 'Bearer ' + $localStorage.access_token }
                }
            },
            updateText: {
                method: 'POST',
                url: '/api/text/update',
                headers: { 
                    'Accept': 'application/json',
                    'Authorization': function (config) { return 'Bearer ' + $localStorage.access_token }
                }
            },
        });
    }
]).service('Text', ["$q", "TextResource", function ($q, TextResource) {

    function getText(page) {
        var deferred = $q.defer();

        var request = new TextResource();

        request.$getText({ page: page }, function (response) {
            deferred.resolve(response);
        }, function (response) {
            deferred.reject();
        });

        return deferred.promise;
    }

    function getPages() {
        var deferred = $q.defer();

        var request = new TextResource();

        request.$getPages({}, function (response) {
            deferred.resolve(response);
        }, function (response) {
            deferred.reject();
        });

        return deferred.promise;
    }

    function updateText(page, text) {
        var deferred = $q.defer();

        var request = new TextResource();
        request.page = page;
        request.text = text;

        request.$updateText(function (response) {
            deferred.resolve(response);
        }, function (response) {
            deferred.reject();
        });

        return deferred.promise;
    }

    return {
        "getText": getText,
        "getPages": getPages,
        "updateText": updateText
    };
}]);