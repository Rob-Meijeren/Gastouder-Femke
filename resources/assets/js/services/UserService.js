angular.module('UserService', ['ngResource', 'ngStorage']).factory('UserResource', ['$resource', '$localStorage',
    function($resource, $localStorage) {
        return $resource('/api/user', {}, {
            login: {
                method: 'POST',
                url: '/api/user/login'
            },
            updatePassword: {
                method: 'POST',
                url: '/api/user/updatePassword',
                headers: { 
                    'Accept': 'application/json',
                    'Authorization': function (config) { return 'Bearer ' + $localStorage.access_token }
                }
            },
            create: {
                method: 'POST',
                url: '/api/user/create',
                headers: { 
                    'Accept': 'application/json',
                    'Authorization': function (config) { return 'Bearer ' + $localStorage.access_token }
                }
            },
            validate: {
                method: 'POST',
                url: '/api/user/validate',
                headers: { 
                    'Accept': 'application/json',
                    'Authorization': function (config) { return 'Bearer ' + $localStorage.access_token }
                }
            },
            getUser: {
                method: 'GET',
                url: '/api/user/user',
                headers: { 
                    'Accept': 'application/json',
                    'Authorization': function (config) { return 'Bearer ' + $localStorage.access_token }
                }
            },
            update: {
                method: 'POST',
                url: '/api/user/update',
                headers: { 
                    'Accept': 'application/json',
                    'Authorization': function (config) { return 'Bearer ' + $localStorage.access_token }
                }
            }
        });
    }
]).service('User', ['$q', '$localStorage', 'UserResource', function ($q, $localStorage, UserResource) {

        function login(email, password) {
            var deferred = $q.defer();

            var request = new UserResource();
            request.username = email;
            request.password = password;

            return request.$login(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject();
            });

            return deferred.promise;
        }

        function updatePassword(userid, currentPassword, newPassword) {
            var deferred = $q.defer();

            var request = new UserResource();
            request.id = userid;
            request.current = currentPassword;
            request.new = newPassword;

            return request.$updatePassword(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject();
            });
        };

        function create (naam, email, wachtwoord) {
            var deferred = $q.defer();

            var request = new UserResource();
            request.naam = naam;
            request.email = email;
            request.wachtwoord = wachtwoord;

            return request.$create(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject();
            });

            return deferred.promise;
        };

        function validate() {
            var deferred = $q.defer();

            if($localStorage.access_token) {
                var request = new UserResource();

                return request.$validate(function (response) {
                    deferred.resolve(response);
                }, function (response) {
                    deferred.reject();
                });
            }
            else {
                deferred.reject();
            }

            return deferred.promise;
        }

        function getUser() {
            var deferred = $q.defer();
            
            var request = new UserResource();

            return request.$getUser({}, function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject();
            });

            return deferred.promise;
        }

        function updateAccount(naam, email) {
            var deferred = $q.defer();
            
            var request = new UserResource();
            request.naam = naam;
            request.email = email;

            return request.$update(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject();
            });

            return deferred.promise;
        }

        return {
            "login": login,
            "updatePassword": updatePassword,
            "create": create,
            "validate": validate,
            "getUser": getUser,
            "update": updateAccount
        };
}]);;
