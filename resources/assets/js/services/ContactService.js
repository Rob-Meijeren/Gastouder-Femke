angular.module('ContactService', ['ngResource']).factory('ContactResource', ['$resource',
    function($resource) {
        return $resource('/contact', {}, {
            contact: {
                method: 'POST',
                url: '/api/contact'
            }
        });
    }
]).service('Contact', ["$q", 'ContactResource', function($q, ContactResource) {

    function contact(Naam, Email, Telefoonnr, Onderwerp, Bericht) {

        var deferred = $q.defer();

        var request = new ContactResource();
        request.naam = Naam;
        request.email = Email;
        request.telefoon = Telefoonnr;
        request.onderwerp = Onderwerp;
        request.bericht = Bericht;

        return request.$contact(function (response) {
            deferred.resolve(response);
        }, function (response) {
            deferred.reject();
        });

        return deferred.promise;
    }

    return {
        "contact": contact
    };
}]);;
