angular.module('AvailabilityService', ['ngResource']).factory('AvailabilityResource', ['$resource', '$localStorage',
    function($resource, $localStorage) {
        return $resource('/api/availability', {}, {
            getAvailability: {
                method: 'GET',
                url: '/api/availability'
            },
            updateDay: {
                method: 'POST',
                url: '/api/availability/update',
                headers: { 
                    'Accept': 'application/json',
                    'Authorization': function (config) { return 'Bearer ' + $localStorage.access_token }
                }
            }
        });
    }
]).service('Availability', ["$q", "AvailabilityResource", function ($q, AvailabilityResource) {

    function getAvailability() {
        var deferred = $q.defer();

        var request = new AvailabilityResource();

        request.$getAvailability({}, function (response) {
            deferred.resolve(response);
        }, function (response) {
            deferred.reject();
        });

        return deferred.promise;
    }

     function updateDay(day) {
        var deferred = $q.defer();

        var request = new AvailabilityResource();
        request.day = day.day;
        request.morning = day.morning;
        request.afternoon = day.afternoon;
        request.evening = day.evening;

        request.$updateDay(function (response) {
            deferred.resolve(response);
        }, function (response) {
            deferred.reject();
        });

        return deferred.promise;
    }

    return {
        "getAvailability": getAvailability,
        "updateDay": updateDay
    };
}]);