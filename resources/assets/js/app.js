angular.module('Femke', [
  'appRoutes',
  'AlbumController',
  'MainController',
  'ContactController',
  'UserController',
  'ErrorController',
  'NavbarController',
  'TekstenController',
  'AccountController'
]);
