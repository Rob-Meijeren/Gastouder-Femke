angular.module('Femke').filter('parseDate', function ($filter) {
  return function(input) {
  	var newDate = new Date(input);
    return $filter('date')(newDate, "dd-MM-yyyy hh:mm");
  };
});