var elixir = require('laravel-elixir');
var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var mainBowerFiles = require('main-bower-files');
require('laravel-elixir-livereload');

var files = mainBowerFiles('**/*.js');

gulp.src(files)
.pipe(concat('vendor.js'))
.pipe(uglify())
.pipe(gulp.dest('public/js'));
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
  mix
    .less(
      '../../../bower_components/bootstrap/less/bootstrap.less'
    )
    .styles([
      'style.css'
    ])
    .scripts([
      'app.js',
      'appRoutes.js',
      'services/**/*.js',
      'controllers/**/*.js',
      'directives/**/*.js',
      'filters/**/*.js'
    ]).livereload([
      'resources/assets/js/**/*.js',
      'resources/assets/css/**/*.css',
    ]);
});