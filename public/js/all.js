angular.module('Femke', [
  'appRoutes',
  'AlbumController',
  'MainController',
  'ContactController',
  'UserController',
  'ErrorController',
  'NavbarController',
  'TekstenController',
  'AccountController'
]);

angular.module('appRoutes', ['ngRoute', 'UserService']).config(['$routeProvider', '$locationProvider',
    function ($routeProvider, $locationProvider) {
        var templateHome = '/views';

        var authenticate = ['$window', '$localStorage', 'User', function ($window, $localStorage, userService) {
            return userService.validate().then(function (response) {
                if(!response.authenticated) {
                    delete $window.sessionStorage.loggedIn;
                    delete $localStorage.access_token;
                    return '/';
                }
                else {
                    return undefined;
                }
            }, function (error) {
                return '/';
            });
        }]

        $routeProvider
        .when('/', {
            templateUrl: templateHome + '/home',
            controller: 'MainController',
            controllerAs: 'vm'
        })
        .when('/overfemke', {
            templateUrl: templateHome + '/about',
            controller: 'MainController',
            controllerAs: 'vm'
        })
        .when('/opvanglocatie', {
            templateUrl: templateHome + '/daycare',
            controller: 'MainController',
            controllerAs: 'vm'
        })
        .when('/activiteiten', {
            templateUrl: templateHome + '/activities',
            controller: 'MainController',
            controllerAs: 'vm'
        })
        .when('/fotoboek', {
            templateUrl: templateHome + '/album_overzicht',
            controller: 'AlbumController',
            controllerAs: 'vm'
        })
        .when('/album/:albumId', {
            templateUrl: templateHome + '/album_detail',
            controller: 'AlbumController',
            controllerAs: 'vm'
        })
        .when('/contact', {
            templateUrl: templateHome + '/contact',
            controller: 'ContactController',
            controllerAs: 'vm'
        })
        .when('/voegAlbumToe', {
            templateUrl: templateHome + '/members/addAlbum',
            controller: 'AlbumController',
            controllerAs: 'vm',
            resolveRedirectTo: authenticate
        })
        .when('/bewerkAlbum/:albumId?', {
            templateUrl: templateHome + '/members/addPhoto',
            controller: 'AlbumController',
            controllerAs: 'vm',
            resolveRedirectTo: authenticate
        })
        .when('/account', {
            templateUrl: templateHome + '/members/account',
            controller: 'AccountController',
            controllerAs: 'vm',
            resolveRedirectTo: authenticate
        })
        .when('/teksten', {
            templateUrl: templateHome + '/members/teksten',
            controller: 'TekstenController',
            controllerAs: 'vm',
            resolveRedirectTo: authenticate
        })
        .when('/login', {
            templateUrl: templateHome + '/auth/login',
            controller: 'UserController',
            controllerAs: 'vm'
        })
        .otherwise({
            redirectTo: '/'
        });

        $locationProvider.html5Mode({
            enabled: true
        });
    }
]);

angular.module('AlbumService', ['ngResource']).factory('AlbumResource', ['$resource',
    function($resource) {
        return $resource('/album', {}, {
            getAllAlbums: {
                method: 'GET',
                url: '/api/album/all'
            },
            getSpecificAlbum: {
                method: 'GET',
                url: '/api/album/:albumId'
            },
            removeAlbum: {
                method: 'POST',
                url: '/api/album/remove'
            },
            updateAlbum: {
                method: 'POST',
                url: '/api/album/update'
            },
            createAlbum: {
                method: 'POST',
                url: '/api/album/create'
            }
        });
    }
]).service('Album', ["$q", "AlbumResource", function ($q, AlbumResource) {

    function getAllAlbums() {
        var deferred = $q.defer();

        var request = new AlbumResource();

        request.$getAllAlbums(function (response) {
            deferred.resolve(response);
        }, function (response) {
            deferred.reject();
        });

        return deferred.promise;
    }

    function getSpecificAlbum(albumId) {
        var deferred = $q.defer();

        var request = new AlbumResource();

        request.$getSpecificAlbum({ albumId:albumId }, function (response) {
            deferred.resolve(response);
        }, function (response) {
            deferred.reject();
        });

        return deferred.promise;
    }

    function removeAlbum(albumId) {
        var deferred = $q.defer();

        var request = new AlbumResource();
        request.albumId = albumId;

        request.$removeAlbum(function (response) {
            deferred.resolve(response);
        }, function (response) {
            deferred.reject();
        });

        return deferred.promise;
    }

    function updateAlbum(albumId, Naam, Omschrijving) {
        var deferred = $q.defer();

        var request = new AlbumResource();
        request.albumID = albumId;
        request.naam = Naam;
        request.omschrijving = Omschrijving;

        request.$updateAlbum(function (response) {
            deferred.resolve(response);
        }, function (response) {
            deferred.reject();
        });

        return deferred.promise;
    }

    function createAlbum(Naam, Omschrijving) {
        var deferred = $q.defer();

        var request = new AlbumResource();
        request.naam = Naam;
        request.omschrijving = Omschrijving;

        request.$createAlbum(function (response) {
            deferred.resolve(response);
        }, function (response) {
            deferred.reject();
        });

        return deferred.promise;
    }

    return {
        "getAllAlbums": getAllAlbums,
        "getSpecificAlbum": getSpecificAlbum,
        "removeAlbum": removeAlbum,
        "updateAlbum": updateAlbum,
        "createAlbum": createAlbum
    };
}]);
angular.module('AvailabilityService', ['ngResource']).factory('AvailabilityResource', ['$resource', '$localStorage',
    function($resource, $localStorage) {
        return $resource('/api/availability', {}, {
            getAvailability: {
                method: 'GET',
                url: '/api/availability'
            },
            updateDay: {
                method: 'POST',
                url: '/api/availability/update',
                headers: { 
                    'Accept': 'application/json',
                    'Authorization': function (config) { return 'Bearer ' + $localStorage.access_token }
                }
            }
        });
    }
]).service('Availability', ["$q", "AvailabilityResource", function ($q, AvailabilityResource) {

    function getAvailability() {
        var deferred = $q.defer();

        var request = new AvailabilityResource();

        request.$getAvailability({}, function (response) {
            deferred.resolve(response);
        }, function (response) {
            deferred.reject();
        });

        return deferred.promise;
    }

     function updateDay(day) {
        var deferred = $q.defer();

        var request = new AvailabilityResource();
        request.day = day.day;
        request.morning = day.morning;
        request.afternoon = day.afternoon;
        request.evening = day.evening;

        request.$updateDay(function (response) {
            deferred.resolve(response);
        }, function (response) {
            deferred.reject();
        });

        return deferred.promise;
    }

    return {
        "getAvailability": getAvailability,
        "updateDay": updateDay
    };
}]);
angular.module('ContactService', ['ngResource']).factory('ContactResource', ['$resource',
    function($resource) {
        return $resource('/contact', {}, {
            contact: {
                method: 'POST',
                url: '/api/contact'
            }
        });
    }
]).service('Contact', ["$q", 'ContactResource', function($q, ContactResource) {

    function contact(Naam, Email, Telefoonnr, Onderwerp, Bericht) {

        var deferred = $q.defer();

        var request = new ContactResource();
        request.naam = Naam;
        request.email = Email;
        request.telefoon = Telefoonnr;
        request.onderwerp = Onderwerp;
        request.bericht = Bericht;

        return request.$contact(function (response) {
            deferred.resolve(response);
        }, function (response) {
            deferred.reject();
        });

        return deferred.promise;
    }

    return {
        "contact": contact
    };
}]);;

angular.module('PhotoService', ['ngResource']).factory('PhotoResource', ['$resource', '$localStorage',
    function($resource, $localStorage) {
        return $resource('/api/photo', {}, {
            getPhotosOfAlbum: {
                method: 'GET',
                url: '/api/photo/:albumId'
            },
            uploadPhoto: {
                method: 'POST',
                url: '/api/photo/add',
                headers: { 
                    'Accept': 'application/json',
                    'Authorization': function (config) { return 'Bearer ' + $localStorage.access_token }
                }
            },
            removePhoto: {
                method: 'POST',
                url: '/api/photo/remove',
                headers: { 
                    'Accept': 'application/json',
                    'Authorization': function (config) { return 'Bearer ' + $localStorage.access_token }
                }
            },
            getRandomPhotos: {
                method: 'GET',
                url: '/api/photo/random'
            }
        });
    }
]).service('Photo', ['$q', 'PhotoResource', function ($q, PhotoResource) {

        function getPhotosOfAlbum(albumId) {
            var deferred = $q.defer();

            var request = new PhotoResource();

            return request.$getPhotosOfAlbum({ albumId:albumId }, function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject();
            });

            return deferred.promise;
        }

        function uploadPhoto(albumId, photo) {
            var deferred = $q.defer();

            var request = new PhotoResource();
            request.albumId = albumId;
            request.photo = photo;

            return request.$uploadPhoto(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject();
            });

            return deferred.promise;
        }

        function removePhoto(photoId) {
            var deferred = $q.defer();

            var request = new PhotoResource();
            request.photoId = photoId;

            return request.$removePhoto(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject();
            });

            return deferred.promise;
        }

        function getRandomPhotos(amountOfPhotos) {
            var deferred = $q.defer();

            var request = new PhotoResource();
            request.numberOfPhotos = amountOfPhotos;

            return request.$getRandomPhotos(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject();
            });

            return deferred.promise;
        }

        return {
            "getPhotosOfAlbum": getPhotosOfAlbum,
            "uploadPhoto": uploadPhoto,
            "removePhoto": removePhoto,
            "getRandomPhotos": getRandomPhotos
        };
}]);
angular.module('TextService', ['ngResource']).factory('TextResource', ['$resource', '$localStorage',
    function($resource, $localStorage) {
        return $resource('/text', {}, {
            getText: {
                method: 'GET',
                url: '/api/text/:page',
                headers: { 
                    'Accept': 'application/json',
                    'Authorization': function (config) { return 'Bearer ' + $localStorage.access_token }
                }
            },
            getPages: {
                method: 'GET',
                url: '/api/text/sections',
                headers: { 
                    'Accept': 'application/json',
                    'Authorization': function (config) { return 'Bearer ' + $localStorage.access_token }
                }
            },
            updateText: {
                method: 'POST',
                url: '/api/text/update',
                headers: { 
                    'Accept': 'application/json',
                    'Authorization': function (config) { return 'Bearer ' + $localStorage.access_token }
                }
            },
        });
    }
]).service('Text', ["$q", "TextResource", function ($q, TextResource) {

    function getText(page) {
        var deferred = $q.defer();

        var request = new TextResource();

        request.$getText({ page: page }, function (response) {
            deferred.resolve(response);
        }, function (response) {
            deferred.reject();
        });

        return deferred.promise;
    }

    function getPages() {
        var deferred = $q.defer();

        var request = new TextResource();

        request.$getPages({}, function (response) {
            deferred.resolve(response);
        }, function (response) {
            deferred.reject();
        });

        return deferred.promise;
    }

    function updateText(page, text) {
        var deferred = $q.defer();

        var request = new TextResource();
        request.page = page;
        request.text = text;

        request.$updateText(function (response) {
            deferred.resolve(response);
        }, function (response) {
            deferred.reject();
        });

        return deferred.promise;
    }

    return {
        "getText": getText,
        "getPages": getPages,
        "updateText": updateText
    };
}]);
angular.module('UserService', ['ngResource', 'ngStorage']).factory('UserResource', ['$resource', '$localStorage',
    function($resource, $localStorage) {
        return $resource('/api/user', {}, {
            login: {
                method: 'POST',
                url: '/api/user/login'
            },
            updatePassword: {
                method: 'POST',
                url: '/api/user/updatePassword',
                headers: { 
                    'Accept': 'application/json',
                    'Authorization': function (config) { return 'Bearer ' + $localStorage.access_token }
                }
            },
            create: {
                method: 'POST',
                url: '/api/user/create',
                headers: { 
                    'Accept': 'application/json',
                    'Authorization': function (config) { return 'Bearer ' + $localStorage.access_token }
                }
            },
            validate: {
                method: 'POST',
                url: '/api/user/validate',
                headers: { 
                    'Accept': 'application/json',
                    'Authorization': function (config) { return 'Bearer ' + $localStorage.access_token }
                }
            },
            getUser: {
                method: 'GET',
                url: '/api/user/user',
                headers: { 
                    'Accept': 'application/json',
                    'Authorization': function (config) { return 'Bearer ' + $localStorage.access_token }
                }
            },
            update: {
                method: 'POST',
                url: '/api/user/update',
                headers: { 
                    'Accept': 'application/json',
                    'Authorization': function (config) { return 'Bearer ' + $localStorage.access_token }
                }
            }
        });
    }
]).service('User', ['$q', '$localStorage', 'UserResource', function ($q, $localStorage, UserResource) {

        function login(email, password) {
            var deferred = $q.defer();

            var request = new UserResource();
            request.username = email;
            request.password = password;

            return request.$login(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject();
            });

            return deferred.promise;
        }

        function updatePassword(userid, currentPassword, newPassword) {
            var deferred = $q.defer();

            var request = new UserResource();
            request.id = userid;
            request.current = currentPassword;
            request.new = newPassword;

            return request.$updatePassword(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject();
            });
        };

        function create (naam, email, wachtwoord) {
            var deferred = $q.defer();

            var request = new UserResource();
            request.naam = naam;
            request.email = email;
            request.wachtwoord = wachtwoord;

            return request.$create(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject();
            });

            return deferred.promise;
        };

        function validate() {
            var deferred = $q.defer();

            if($localStorage.access_token) {
                var request = new UserResource();

                return request.$validate(function (response) {
                    deferred.resolve(response);
                }, function (response) {
                    deferred.reject();
                });
            }
            else {
                deferred.reject();
            }

            return deferred.promise;
        }

        function getUser() {
            var deferred = $q.defer();
            
            var request = new UserResource();

            return request.$getUser({}, function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject();
            });

            return deferred.promise;
        }

        function updateAccount(naam, email) {
            var deferred = $q.defer();
            
            var request = new UserResource();
            request.naam = naam;
            request.email = email;

            return request.$update(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject();
            });

            return deferred.promise;
        }

        return {
            "login": login,
            "updatePassword": updatePassword,
            "create": create,
            "validate": validate,
            "getUser": getUser,
            "update": updateAccount
        };
}]);;

angular.module('AccountController', ['UserService', 'AvailabilityService'])
.controller('AccountController', ['$rootScope', '$scope', 'User', 'Availability', AccountController]);

function AccountController ($rootScope, $scope, UserService, AvailabilityService) {
	var vm = this;

	vm.user;
	vm.currentPassword;
	vm.newPassword;
	vm.passwordConfirmation;
	vm.email;
	vm.naam;
	vm.days;

	vm.getAvailability = function () {
        AvailabilityService.getAvailability().then(function (response) {
            vm.days = response.days;
        });
    };

    vm.updateDay = function (day) {
    	AvailabilityService.updateDay(day).then(function (response) {
    		$rootScope.result = response.result;
    	});
    }

	vm.getAuthenticatedUser = function () {
		UserService.getUser().then(function (response) {
			vm.user = response.user;
		}, function (error) {
			$rootScope.result = error;
		})
	}

	vm.updateAccount = function () {
		UserService.update(vm.user.name, vm.user.email).then(function (response) {
			$rootScope.result = response.result;
		}, function (error) {
			$rootScope.result = error;
		});
	};

	vm.updatePassword = function () {
		UserService.updatePassword(vm.user.id, vm.currentPassword, vm.newPassword).then(function (response) {
			if(response.error) {
				$rootScope.result = response.error;
			}
			else if(response.result) {
				$rootScope.result = response.result;
				vm.currentPassword = '';
				vm.newPassword = '';
				vm.passwordConfirmation = '';
			}
		}, function (error) {
			$rootScope.result = error;
		});
	};

	vm.registreer = function () {
		debugger;
		UserService.create(vm.naam, vm.email, vm.newPassword).then(function (response) {
			$rootScope.result = 'Gebruiker aangemaakt';
		}, function (error) {
			$rootScope.result = error;
		});
	}
}
angular.module('AlbumController', ['AlbumService', 'PhotoService', 'naif.base64', 'ui.bootstrap', 'ngAnimate', 'ngTouch'])
    .controller('AlbumController', ['$rootScope', '$scope', '$location', '$routeParams', '$q', '$window', 'Album', 'Photo', AlbumController]);

function AlbumController($rootScope, $scope, $location, $routeParams, $q, $window, AlbumService, PhotoService) {
    var vm = this;

    vm.albums = [];
    vm.selectedAlbum;
    vm.uploading = false;
    vm.loading = false;
    vm.albumNr = $routeParams.albumId;
    vm.albumPhotos = [];
    vm.albumNaam;
    vm.albumOmschrijving;
    vm.newPhotos;
    vm.activeSlide = 0;
    vm.thumbnailSize = 5;
 	vm.thumbnailPage = 1;
 	vm.thumbnails;

    vm.getAllAlbums = function() {
        vm.loading = true;
        AlbumService.getAllAlbums().then(function(response) {
            vm.albums = response.albums;
            var photoCalls = [];

            //get photos for each album concurrently
            angular.forEach(vm.albums, function(album, index) {
                var photoCall = getPhotosofAlbum(album).then(function(photos) {
                    album.fotos = photos;
                    if(album.fotos.length > 0){
                        album.fotos[0].active = true;
                    }
                }, function(error) {
                    album.fotos = [error];
                });
                photoCalls.push(photoCall);
            });

            $q.all(photoCalls).then(function() {
                vm.loading = false;
            }, function() {
                vm.loading = false;
                $rootScope.result = 'Er is iets fout gegaan met het ophalen van de foto(s). Probeer het nogmaals of contacteer de webmaster';
            });
        }, function(response) {
            $rootScope.result = 'Er is iets fout gegaan met het ophalen van de albums. Laad de pagina nogmaals of contacteer de webmaster.';
        });
    };

    vm.getSpecificAlbum = function(albumId) {
        var albumID = albumId || vm.albumNr;
        AlbumService.getSpecificAlbum(albumID).then(function(response) {
            vm.selectedAlbum = response.album;

            getPhotosofAlbum(vm.selectedAlbum).then(function(photos) {
                vm.selectedAlbum.fotos = photos;
                vm.setThumbNails();
            });
        });
    };

    vm.addPhotoToAlbum = function(albumID, photos) {
        var promises = [];
        // upload a photo to the backend with the albumid when multiple pictures make multiple calls
        vm.uploading = true;
        angular.forEach(photos, function(photo) {
            var promise = PhotoService.uploadPhoto(albumID, photo).then(function(response) {
                if (typeof vm.selectedAlbum !== 'undefined') {
                    vm.selectedAlbum.fotos.push(response.photo);
                }
            });
            promises.push(promise);
        });

        $q.all(promises).then(function() {
            vm.uploading = false;
        }, function() {
            vm.uploading = false;
            $rootScope.result = 'Er is iets fout gegaan met het toevoegen van de foto(s). Probeer het nogmaals of contacteer de webmaster';
        })
    };

    vm.removePhotoFromAlbum = function(photo) {
        PhotoService.removePhoto(photo.id).then(function(response) {
            vm.selectedAlbum.fotos.splice(vm.selectedAlbum.fotos.indexOf(photo), 1);
        }, function(error) {
            $rootScope.result = error;
        });
    };

    vm.createAlbum = function() {
        // send the name and description of the newly to be created album to the backend
        AlbumService.createAlbum(vm.albumNaam, vm.albumOmschrijving).then(function(response) {
            vm.addPhotoToAlbum(response.albumID, vm.albumPhotos);
            $location.path('/bewerkAlbum/' + response.albumID);
        }, function(error) {
            console.log('Error');
        });
    };

    vm.prevPage = function(){
		if (vm.thumbnailPage > 1){
		  vm.thumbnailPage--;
		}
		vm.setThumbNails();
	}

	vm.nextPage = function(){
		if (vm.thumbnailPage <= Math.floor(vm.selectedAlbum.fotos.length/vm.thumbnailSize)){
		  vm.thumbnailPage++;
		}
		vm.setThumbNails();
	}

	vm.setThumbNails = function () {
  		vm.thumbnails = vm.selectedAlbum.fotos.slice((vm.thumbnailPage - 1) * vm.thumbnailSize, vm.thumbnailPage * vm.thumbnailSize);
  	}

	vm.setActive = function(idx) {
		vm.activeSlide = idx + ((vm.thumbnailPage - 1) * vm.thumbnailSize);
	}

    function getPhotosofAlbum(album) {
        var deferred = $q.defer();

        // get the photos of the album and add them to the album.
        PhotoService.getPhotosOfAlbum(album.id).then(function(response) {
            deferred.resolve(response.fotos);
        }, function(error) {
            deferred.reject('Niet alle fotos konden worden opgehaald.');
        });

        return deferred.promise;
    };
};

angular.module('ContactController', ['ContactService']).controller('ContactController', ['$rootScope', 'Contact', ContactController]);

function ContactController ($rootScope, ContactService) {
	var vm = this;

    vm.Naam;
	vm.Email;
	vm.Telefoon;
	vm.Onderwerp;
	vm.Bericht;

	vm.sendContactForm = function () {
		ContactService.contact(vm.Naam, vm.Email, vm.Telefoon, vm.Onderwerp, vm.Bericht).then(function () {
			$rootScope.result = 'Uw Email is verzonden. Er word zo snel mogelijk contact met u opgenomen.';
		}, function () {
			$rootScope.result = 'Er is een fout opgetreden bij het verzenden van uw email. Weet u zeker dat u de verplichte velden hebt ingevuld? Probeer het later nog eens. Blijft het probleem bestaan kunt u de webmaster bereiken op webmaster@vmtr.nl';
		});
	};
};
angular.module('ErrorController', []).controller('ErrorController', ['$rootScope', '$timeout', ErrorController]);

function ErrorController ($rootScope, $timeout) {
	$rootScope.result;

	$rootScope.$watch('result', function() {
        $timeout(function () {
        	if($rootScope.result !== '') {
        		$rootScope.result = '';
        	}
        }, 10000);
    });
}
var app = angular.module('MainController', ['ngSanitize', 'UserService', 'PhotoService', 'TextService', 'AvailabilityService'])

app.controller('MainController', ['$sce', '$localStorage', 'User', 'Photo', 'Text', 'Availability', MainController]);

function MainController ($sce, $localStorage, UserService, PhotoService, TextService, AvailabilityService) {
    var vm = this;

    vm.photos = [];

    vm.getRandomPictures = function () {
        PhotoService.getRandomPhotos(5).then(function (response) {
            vm.photos = response.photos;
        });
    };

    vm.getPictureSrc = function (value) {
        if(value == 0) {
            return '/img/kruisje.png';
        }
        else {
            return '/img/vinkje.png';
        }
    };

    vm.getText = function (page) {
        TextService.getText(page).then(function (response) {
            vm.text =  $sce.trustAsHtml(response.text);
        });
    };

    vm.getAvailability = function () {
        AvailabilityService.getAvailability().then(function (response) {
            vm.days = response.days;
        })
    };

/*    if($localStorage.access_token) {
        
    }*/
};
angular.module('NavbarController', []).controller('NavbarController', ['$scope', '$window', '$localStorage', NavbarController]);

function NavbarController($scope, $window, $localStorage) {
	var vm = this; 

	$scope.$watch(function () {
        return $window.sessionStorage.loggedIn 
    }, function(n,o){
        vm.setLoggedIn();
    });

    vm.setLoggedIn = function () {
    	$scope.loggedIn = $window.sessionStorage.loggedIn;
    }

    vm.logout = function () {
    	delete $window.sessionStorage.loggedIn;
    	delete $localStorage.access_token;
    	delete $localStorage.refresh_token;
    }

    vm.setLoggedIn();
}
angular.module('TekstenController', ['TextService'])
.controller('TekstenController', ['$rootScope', '$scope', 'Text', TekstenController]);

function TekstenController ($rootScope, $scope, textService) {
	var vm = this;

	vm.pages = [];
	vm.selectedPage;
	vm.text;

	vm.getSections = function () {
		textService.getPages().then(function (response) {
			vm.pages = response.pages;
		}, function (error) {
			$rootScope.result = error;
		});
	};

	vm.getTextOfSection = function () {
		textService.getText(vm.selectedPage).then(function (response) {
			vm.text = response.text;
		}, function (error) {
			$rootScope.result = error;
		});
	}

	vm.updateText = function () {
		textService.updateText(vm.selectedPage, vm.text).then(function (response) {
			$rootScope.result = response.updated;
		}, function (error) {
			$rootScope.result = error;
		});
	};

	$scope.$watch('vm.selectedPage', function () {
		vm.getTextOfSection();
	});
}
angular.module('UserController', ['UserService', 'ngStorage'])
    .controller('UserController', ['$rootScope', '$window', '$scope', '$localStorage', '$location', 'User', UserController]);

function UserController ($rootScope, $window, $scope, $localStorage, $location, userService) {
    var vm = this;
    vm.email;
    vm.password;
    vm.result;

    vm.login = function () {
        userService.login(vm.email, vm.password).then(function (response) {
            if(response.login_error){
                $rootScope.result = response.login_error;
            }
            else {
                $localStorage.access_token = response.access_token;
                $window.sessionStorage.loggedIn = true;
                $location.path('/');
            }
        }, function (error) {
            $rootScope.result = 'Deze combinatie van email en wachtwoord bestaat niet in de database. Probeer het nogmaals met de juiste inloggegevens';
        })
    };
};

angular.module('Femke').directive("passwordVerify", function() {
   return {
      require: "ngModel",
      scope: {
        passwordVerify: '='
      },
      link: function(scope, element, attrs, ctrl) {
        scope.$watch(function() {
            var combined;

            if (scope.passwordVerify || ctrl.$viewValue) {
               combined = scope.passwordVerify + '_' + ctrl.$viewValue; 
            }                    
            return combined;
        }, function(value) {
            if (value) {
                ctrl.$parsers.unshift(function(viewValue) {
                    var origin = scope.passwordVerify;
                    if (origin !== viewValue) {
                        ctrl.$setValidity("passwordVerify", false);
                        return undefined;
                    } else {
                        ctrl.$setValidity("passwordVerify", true);
                        return viewValue;
                    }
                });
            }
        });
     }
   }
});
angular.module('Femke').filter('parseDate', function ($filter) {
  return function(input) {
  	var newDate = new Date(input);
    return $filter('date')(newDate, "dd-MM-yyyy hh:mm");
  };
});
//# sourceMappingURL=all.js.map
