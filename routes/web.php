<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

// Catch all view routes. Always gotta stay at the bottom since order of routes matters.
Route::get('/views/{name}', function($name) {
  $view_path = 'partials.' . $name;

  if (View::exists($view_path)) {
    return view($view_path);
  }

  App::abort(404);
});

Route::get('/views/{folder}/{name}', function($folder, $name) {
  $view_path = 'partials.' . $folder . '.' . $name;

  if (View::exists($view_path)) {
      return view($view_path);
  }

  App::abort(404);
});

// Using different syntax for Blade to avoid conflicts with AngularJS.
// You are well-advised to go without any Blade at all.
Blade::setContentTags('<%', '%>'); // For variables and all things Blade.
Blade::setEscapedContentTags('<%%', '%%>'); // For escaped data.

