<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['api']], function () {
	Route::group(['prefix' => 'photo'], function () {
		Route::get('random', 'PhotoController@getRandomPhotos');
		Route::post('delete', 'PhotoController@removePhoto');
		Route::post('add', 'PhotoController@uploadPhoto');
		Route::get('{albumId}', 'PhotoController@getPhotosOfAlbum');
	});

	Route::group(['prefix' => 'album'], function () {
		Route::post('create', 'AlbumController@addAlbum');
		Route::post('edit', 'AlbumController@showEditAlbum');
		Route::get('all', 'AlbumController@getAllAlbums');
		Route::post('updateGeneralAlbum', 'AlbumController@updateAlbum');
		Route::get('{id}', 'AlbumController@getAlbum');
	});

	Route::group(['prefix' => 'user'], function () {
		Route::post('validate', 'LogController@validateToken');
		Route::post('login', 'LogController@login');
		Route::get('user', 'UserController@getUser');
		Route::post('update', 'UserController@updateAccount');
		Route::post('updatePassword', 'UserController@updatePassword');
		Route::post('create', 'LogController@registreer');
	});

	Route::group(['prefix' => 'text'], function () {
		Route::get('sections', 'TextController@getAllPages');
		Route::get('{page}', 'TextController@getText');
		Route::post('update', 'TextController@updateText');
	});

	Route::get('availability', 'HomeController@getAvailability');
	Route::post('availability/update', 'HomeController@updateAvailability');
	Route::post('contact', 'HomeController@sendMessage');	
});